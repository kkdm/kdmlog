import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.kdmlog",
      scalaVersion := "2.12.6",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "kdmlog",
    libraryDependencies += "com.typesafe.akka" %% "akka-http"   % "10.1.3",
    libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.12",
    libraryDependencies += "org.mariadb.jdbc" % "mariadb-java-client" % "2.3.0",
    libraryDependencies += "joda-time" % "joda-time" % "2.10",
    libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.5",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
  ).
  enablePlugins(SbtTwirl)

unmanagedResourceDirectories in Compile += baseDirectory.value / "src/main/twirl"
includeFilter in (Compile, unmanagedResources) := "*.html" || "*.css"

