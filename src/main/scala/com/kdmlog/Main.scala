package com.kdmlog

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.directives._
import play.twirl.api.Html
import StatusCodes._
import Directives._
import ContentTypeResolver.Default

import scala.io.StdIn
import scala.util.matching.Regex
import scala.util.{Success, Failure}

import com.kdmlog.marshal.{ApiNewPost, ApiUpdatePost}
import com.kdmlog.marshal.JsonSupport._
import com.kdmlog.marshal.Marshal._
import com.kdmlog.msg._
import com.kdmlog.msg.Parser._
import com.kdmlog.auth.Auth._

object Main extends App {

  implicit val system = ActorSystem("my-sys")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  // To handle unexpect request
  implicit val rejections = 
    RejectionHandler.newBuilder()
      .handleNotFound { extractUnmatchedPath { p => complete((404, html.error("404", parseErrorCode(404, p.toString).msg))) }} 
      .handleAll[MethodRejection] { mr => complete((MethodNotAllowed, html.error("405", parseErrorCode(405, "").msg))) }
      .result()

  // Routes setting.
  val routes =
    pathSingleSlash { get { complete(html.index("Top", DBAccessor.getIndexArticles(5))) } } ~
    path("contact") { get { complete(html.contact("Contact")) } } ~
    path("healthcheck") { get {complete("alive.") }} ~
    path("posts" / Segment) { postId => 
      get { 
        DBAccessor.getArticleFromId(postId) match {
          case Some(p) => complete(html.post(p))
          case None => complete(404, html.error("404", parseErrorCode(404, s"posts/${postId}").msg)) 
        }
      } ~
      post { formFields("name", "email", "comment") { (name, email, comment) =>
        DBAccessor.postComment(postId, (name, email, comment)) match {
          case Success(_) => { DBAccessor.getArticleFromId(postId) match {
            case Some(p) => complete(Created, html.post(p))
            case None => complete(InternalServerError, html.error("500", parseErrorCode(500, "").msg))
          }}
          case Failure(e) => {
            println(e.getStackTrace().mkString("\n"))
            complete(InternalServerError, html.error("500", parseErrorCode(500, "").msg))
          }
        }
      }}
    } ~
    path("search") {
      get { parameter("keywords") { (tags) =>
        if (tags.trim.isEmpty()) complete(404, html.error(s"No Result", parseErrorCode(0, tags).msg))
        else {
          val articles = DBAccessor.getArticlesFromTags(tags)
          if (articles.length != 0) complete(html.index("Search", articles))
          else complete(404, html.error(s"No Result", parseErrorCode(0, tags).msg))
        }}
      }
    } ~
    pathPrefix("edit") { 
      path("update") { post { entity(as[ApiUpdatePost]) { np => 
        authorize(authAccount(np.user, np.password)) { 
          DBAccessor.updateArticle(np.id, np.title, np.tags, np.body) match {
            case Success(_) => complete(204, "Updated.")
            case Failure(e) => { 
              println(e.getStackTrace().mkString("\n"))
              complete(InternalServerError, "Something went wrong.")
            }
          }
        }}}
      }
      path("new") { post { entity(as[ApiNewPost]) { np => 
        authorize(authAccount(np.user, np.password)) {
          DBAccessor.postArticle(np.title, np.tags, np.body) match {
            case Success(0) => complete(BadRequest, "Invalid request.")
            case Success(x) => complete(Created, "Success.")
            case Failure(e) => {
              println(e.getStackTrace().mkString("\n"))
              complete(InternalServerError, "Something went wrong.")
            }
          }
        }
      }}}
    }

  val bindingFuture = Http().bindAndHandle(routes, "0.0.0.0", 9091)
  println(s"Server listning")
  bindingFuture.onFailure {
    case e: Exception => {
      println(s"Error: ${e}")
      println("Server shuting down")
    }
  }
}
