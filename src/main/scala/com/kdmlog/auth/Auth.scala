package com.kdmlog.auth

object Auth {
  def authAccount(user: String, password: String): Boolean = user == sys.env("API_USER") && password == sys.env("API_PASS")
}
