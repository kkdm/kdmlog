package com.kdmlog.dbentity

case class Tag(postId: Int, name: String)
case class Comment(postId: Int, name: String, body: String, commentDate: String)
case class Post(postId: Int, title: String, body: String, shortBody: String, publishDate: String, updateDate: String, likes: Int)
case class Image(imageId: Int, name: String, url: String)
case class Article(post: Post, tags: Seq[Tag], comments: Seq[Comment])
