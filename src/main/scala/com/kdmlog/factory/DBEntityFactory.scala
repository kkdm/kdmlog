package com.kdmlog.factory

import org.joda.time.format._
import java.sql.ResultSet
import org.joda.time.DateTime

import com.kdmlog.dbentity._

trait DBEntityFactory[A] {
  def create(rs: ResultSet): A
}

object DBEntityFactory {
  final val DATE_FMT = "yyyy-MM-dd HH:mm:ss"
  final val SHORT_BODY_LEN = 50

  implicit val createPost = new DBEntityFactory[Post] {
    def create(rs: ResultSet): Post = {
      def createShortBody(str: String): String = {
        val s = str.replaceAll("\\!\\[.{0,}\\]\\(.{0,}\\)", "").replaceAll("#", "").replaceAll("\\*", "")
        if (s.length > 50) s.slice(0, SHORT_BODY_LEN) else s
      }
      def b = rs.getString("body").replaceAll("\r", "")
      def s = createShortBody(b)
      return new Post(
        rs.getInt("post_id"),
        rs.getString("title"),
        b,
        s,
        new DateTime(rs.getTimestamp("publish_date")).toString(DATE_FMT),
        new DateTime(rs.getTimestamp("update_date")).toString(DATE_FMT),
        rs.getInt("likes")
      )
    }
  }

  implicit val createComment = new DBEntityFactory[Comment] {
    def create(rs: ResultSet): Comment = {
      return new Comment(
        rs.getInt("post_id"),
        rs.getString("name"),
        rs.getString("body").replaceAll("\r", ""),
        new DateTime(rs.getTimestamp("comment_date")).toString(DATE_FMT)
      )
    }
  }

  implicit val createTag = new DBEntityFactory[Tag] {
    def create(rs: ResultSet): Tag = new Tag(rs.getInt("post_id"), rs.getString("name"))
  }

  implicit val createImage = new DBEntityFactory[Image] {
    def create(rs: ResultSet): Image = new Image(rs.getInt("image_id"), rs.getString("name"), rs.getString("url"))
  }
}
