package com.kdmlog.marshal

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._
import DefaultJsonProtocol._

case class ApiNewPost(user: String, password: String, title: String, tags: String, body: String) 
case class ApiUpdatePost(id : Int, user: String, password: String, title: String, tags: String, body: String)
object JsonSupport extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val apiNewPostFormat = jsonFormat5(ApiNewPost)
  implicit val apiUpdatePostFormat = jsonFormat6(ApiUpdatePost)
}

