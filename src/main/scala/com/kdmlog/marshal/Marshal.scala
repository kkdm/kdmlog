package com.kdmlog.marshal

import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.model.MediaType
import play.twirl.api.Html

/** Required to convert twirl html template into string. When responses. */
object Marshal {
  implicit val htmlMarshaller = marshaller[Html](`text/html`)
  def marshaller[A <: AnyRef](cType: MediaType): ToEntityMarshaller[A] =
    Marshaller.StringMarshaller.wrap(cType)(_.toString)
}
