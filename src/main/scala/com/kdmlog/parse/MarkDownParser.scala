package com.kdmlog.parse

import scala.util.{Try, Success, Failure}
import com.kdmlog.DBAccessor

object MarkDownParser {
  def removeMarkDownSymbols(target: String): String = target.replaceAll("[#\\*\\>\\[\\]\\!]", "")

  def convert(mdStr: String): String = {
    val hPat = "^(#+) (.+)$".r
    val olPat = "^ *\\d+\\. (.+)$".r
    val ulPat = "^ *[\\-\\*\\+] (.+)$".r
    val yamlPat = "^([a-zA-Z0-9]+.+:)$".r
    val imgPat = "^\\!\\[(.+)\\]\\((\\d+)\\)".r
    val codeStartPat = "^`{3}(.+)$".r
    val codeEndPat = "^`{3}$".r
    val blockQPat = "^>(.+)$".r

    def toImg(name: String, id: Int): String = {
      DBAccessor.getImage(id) match { 
        case Some(x) => s"""<p><img alt="${name}" src="${x.url}"></p>""" 
        case None => "" 
      }
    }

    var closeTag = ""
    var first = true
    val str = mdStr.split("\n").map { line => 
      line match {
        case yamlPat(s) if (closeTag != "</code></pre>") => {
          closeTag = "</ul>"
          s"<p>${s}</p>\n"
        }
        case hPat(hSym, str) if (closeTag != "</code></pre>") => s"<h${hSym.count(_ == '#')}>${str}</h${hSym.count(_ == '#')}>"
        case ulPat(s) if (closeTag != "</code></pre>") => {
          if (first) {
            first = false
            closeTag = if (closeTag == "</ul>") closeTag else "</ul>"
            s"<ul><li>${s.trim}</li>"
          }
          else s"<li>${s.trim}</li>"
        }
        case olPat(s) if (closeTag != "</code></pre>") => {
          if (first) {
            first = false
            closeTag = if (closeTag == "</ol>") closeTag else "</ol>"
            s"<ol><li>${s.trim}</li>"
          }
          else s"<li>${s.trim}</li>"
        }
        case imgPat(name, id) if (closeTag != "</code></pre>") => toImg(name, id.toInt)
        case codeStartPat(tp) => {
          closeTag = "</code></pre>"
          s"""<pre><code class="${tp}">\n"""
        }
        case codeEndPat(_*) => {
          first = true
          val ret = s"${closeTag}\n"
          closeTag = ""
          ret
        }
        case blockQPat(s) if (closeTag != "</code></pre>") => {
          if (closeTag == "</blockquote>") s"${s.trim}<br>" 
          else {
            first = false
            closeTag = "</blockquote>"
            s"<blockquote>${s.trim}<br>"
          }
        }
        case s => {
          if (closeTag == "</code></pre>") s"${s}\n" 
          else if (s.isEmpty()) {
            if (closeTag != "") {
              first = true
              val ret = s"${closeTag}\n"
              closeTag = ""
              ret
            } 
            else "\n"
          }
          else {
            first = false
            closeTag = "</p>"
            s"<p>${s}"
          }
        }
      }
    }.mkString + closeTag 

    def toLink(str: String): String = {
      val linkPat = """([^\!])\[([^\[\]\(\)]+)\]\(([^\(\)\[\]]+)\)"""
      linkPat.r.replaceAllIn(str, m => s"""${m.group(1)}<a href="${m.group(3)}" target="_blank">${m.group(2)}</a>""")
    }
    def toEm(str: String): String = "([^\\*_])[\\*_]([^\\*_\\n<>]+)[\\*_]([^\\*_])".r.replaceAllIn(str, m => s"${m.group(1)}<em>${m.group(2)}</em>${m.group(3)}")
    def toStrong(str: String): String = "[\\*_]{2}([^\\*_\\n<>]+)[\\*_]{2}".r.replaceAllIn(str, m => s"<strong>${m.group(1)}</strong>")
    def toCode(str: String): String = "`{1}([^`\\n<>]+)`{1}".r.replaceAllIn(str, m => s"<code>${m.group(1)}</code>") 
    toCode(toStrong(toEm(toLink(str))))
  }
}
