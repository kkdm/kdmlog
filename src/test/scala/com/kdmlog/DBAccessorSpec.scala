package com.kdmlog

import org.scalatest._
import scala.io.Source
import scala.util.{Try, Success, Failure}
import com.kdmlog.dbentity._

class DBAccessorSpec extends FunSuite {
  val dataPath = "src/test/scala/com/kdmlog/data/"
  val className = getClass.getSimpleName
  val testDataPath = s"${dataPath}/${className}"

  def checkEnvs: Try[Boolean] = Try { !sys.env("DB_USER").isEmpty && !sys.env("DB_PW").isEmpty && !sys.env("MYSQL_URL").isEmpty }

  def getData(fileName: String): String = Source.fromFile(s"${testDataPath}/${fileName}").getLines.mkString("\n")

  ignore ("getArticleFromId should return Article Case class") {
    if (checkEnvs.getOrElse(false)) assert(DBAccessor.getArticleFromId("24").get.isInstanceOf[Article])
    else true
  }
}
